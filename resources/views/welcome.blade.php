<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Deskfinder</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Fugaz+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <link href="/css/app.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<nav class="homepage-nav" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo"><!-- <img src="/logo.png" style="width: 28px;margin: 15px;"> -->Deskfinder</a>
        <ul class="right hide-on-med-and-down">
            <li><a href="#" class="white-text">Explore</a></li>
            <li><a href="#" class="white-text">Locations</a></li>
            <li><a href="#" class="white-text">Blog</a></li>
            <li><a href="#" class="white-text">Login / Register</a></li>
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li><a href="#" class="black-text">Explore</a></li>
            <li><a href="#" class="black-text">Locations</a></li>
            <li><a href="#" class="black-text">Blog</a></li>
            <li><a href="#" class="black-text">Login / Register</a></li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>
<div id="index-banner" class="parallax-container">
    <div class="no-pad-bot">
        <div class="container">
            <div class="slider-content">
                <div class="row center">
                    <div class="col s12">
                        <h5 class="header col s12 light white-text">Lets find your desk!</h5>
                        <p>Search deskfinder for co-working spaces in the uk</p>
                    </div>
                    <div class="col s12">
                        <nav class="search-center">
                            <div class="nav-wrapper">
                              <form>
                                <div class="input-field">
                                  <input id="search" type="search" placeholder="Search your city..." required>
                                  <label for="search"><i class="material-icons">search</i></label>
                                  <i class="material-icons">close</i>
                                </div>
                              </form>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="/bg.jpg" alt="Unsplashed background img 1"></div>
</div>

<div class="grey-section full-width">
    <div class="container">
        <div class="row">
            <div class="col s12">
                Deskfinder app coming soon!
                <a class="waves-effect waves-light btn blue right center-small">Learn more</a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="section">

      <div class="row">
        <div class="col s12 center">
          <h4>Co-Working, On Demand.</h4>
          <p class="light">Our goal at deskfinder is to create an application simular to uber, but for co-working. Simply find a Location, book and pay through the app, then work. </p>
        </div>
      </div>

    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col s12 m6 l4 center">
            <div class="card">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" style="height:200px;" src="/london.jpeg">
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">London<i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">London<i class="material-icons right">close</i></span>
                    <p>Here is some more information about this product that is only revealed once clicked on.</p>
                    <a class="waves-effect waves-light btn blue">Find a desk</a>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l4 center">
            <div class="card">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" style="height:200px;" src="/cambridge.jpeg">
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Cambridge<i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Cambridge<i class="material-icons right">close</i></span>
                    <p>Here is some more information about this product that is only revealed once clicked on.</p>
                    <a class="waves-effect waves-light btn blue">Find a desk</a>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l4 center">
            <div class="card">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" style="height:200px;" src="/norwich.jpeg">
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Norwich<i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">Norwich<i class="material-icons right">close</i></span>
                    <p>Here is some more information about this product that is only revealed once clicked on.</p>
                    <a class="waves-effect waves-light btn blue">Find a desk</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="testimonials">
    <div class="parallax-container valign-wrapper">
        <div class="section no-pad-bot full-width">
          <div class="container">
            <div class="row center">
              <h5 class="header col s12 light center white-text">Search deskfinder for co-working spaces in the uk</h5>
              <!-- <p class="white-text"><strong>person</strong> - company</p> -->
            </div>
          </div>
        </div>
        <div class="parallax"><img src="/testimonials.png" alt="Unsplashed background img 2" style="display: block; transform: translate3d(-50%, 241px, 0px);"></div>
    </div>
</div>


<!-- <div class="container">
    <div class="row">
        <div class="col s12 m4">
            <div class="icon-block">
                <h2 class="center light-blue-text"><i class="material-icons">business</i></h2>
                <h5 class="center">Find a location</h5>

                <p class="center light">We did most of the heavy lifting for you to provide a default stylings that incorporate our custom components. Additionally, we refined animations and transitions to provide a smoother experience for developers.</p>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="icon-block">
                <h2 class="center light-blue-text"><i class="material-icons">payment</i></h2>
                <h5 class="center">Book online and pay</h5>

                <p class="center light">By utilizing elements and principles of Material Design, we were able to create a framework that incorporates components and animations that provide more feedback to users. Additionally, a single underlying responsive system across all platforms allow for a more unified user experience.</p>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="icon-block">
                <h2 class="center light-blue-text"><i class="material-icons">work</i></h2>
                <h5 class="center">Work!</h5>

                <p class="center light">We have provided detailed documentation as well as specific code examples to help new users get started. We are also always open to feedback and can answer any questions a user may have about Materialize.</p>
            </div>
        </div>
    </div>
</div> -->

<footer class="page-footer blue">
    <!-- <div class="container">
        <div class="row">
            <div class="col l4 s12">
                <h5 class="white-text">Deskfinder</h5>
                <ul>
                    <li><a class="white-text" href="#!">Link 1</a></li>
                    <li><a class="white-text" href="#!">Link 2</a></li>
                    <li><a class="white-text" href="#!">Link 3</a></li>
                    <li><a class="white-text" href="#!">Link 4</a></li>
                </ul>
            </div>
            <div class="col l4 s12">
                <h5 class="white-text">Pages</h5>
                <ul>
                    <li><a class="white-text" href="#!">Link 1</a></li>
                    <li><a class="white-text" href="#!">Link 2</a></li>
                    <li><a class="white-text" href="#!">Link 3</a></li>
                    <li><a class="white-text" href="#!">Link 4</a></li>
                </ul>
            </div>
            <div class="col l4 s12">
                <h5 class="white-text">Connect</h5>
                <ul>
                    <li><a class="white-text" href="#!">Facebook</a></li>
                    <li><a class="white-text" href="#!">Twitter</a></li>
                    <li><a class="white-text" href="#!">Google +</a></li>
                    <li><a class="white-text" href="#!">Linkedin</a></li>
                </ul>
            </div>
        </div>
    </div> -->
    <div class="footer-copyright">
        <div class="container">
            &copy; Deskfinder 2015
        </div>
    </div>
</footer>

<script src="/js/app.js"></script>

</body>
</html>

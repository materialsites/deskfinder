(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax(); 
    $(".dropdown-button").dropdown();
    $( ".type-input" ).focus(function() {
      $( ".animation" ).addClass("hidden");
    });

  }); // end of document ready
})(jQuery); // end of jQuery name space
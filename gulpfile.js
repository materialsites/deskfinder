var gulp            = require('gulp');
var less            = require('gulp-less');
var minifycss       = require('gulp-minify-css');
var autoprefixer    = require('gulp-autoprefixer');
var uglify          = require('gulp-uglify');
var concat          = require('gulp-concat');


gulp.task('appCss', function() {
 gulp.src('resources/assets/less/app.less')
     .pipe(less())
     .pipe(autoprefixer('last 10 version'))
     .pipe(minifycss())
     .pipe(gulp.dest('public/css'));
});

gulp.task('appJs', function() {
    gulp.src(['./resources/assets/js/**/*.js'])
    .pipe(uglify())
    .pipe(concat({ path: 'app.js', stat: { mode: 0666 }}))
    .pipe(gulp.dest('./public/js'));

});

gulp.task('watchCss', function() {
 gulp.watch('resources/assets/less/**/*.less', ['appCss']);
});

gulp.task('watchJs', function() {
 gulp.watch('resources/assets/js/**/*.js', ['appJs']); 
});

gulp.task('default', ['watchCss', 'watchJs']);